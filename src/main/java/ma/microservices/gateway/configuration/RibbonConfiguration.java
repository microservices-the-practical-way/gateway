package ma.microservices.gateway.configuration;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.*;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

	// IPing to override the default check-status mechanism
	@Bean
	public IPing ribbonPing(final IClientConfig config) {
		// The PingUrl implementation will check if services are alive.
		// The false flag is just to indicate that the endpoint is not secured.
		return new PingUrl(false, "/health");
	}

	// IRule to change the default load balancing strategy
	@Bean
	public IRule ribbonRule(final IClientConfig config) {
		/*
		 * The AvailabilityFilteringRule is an alternative to the default
		 * RoundRobinRule. It also cycles through the instances but, besides that, it
		 * takes into account the availability being checked by our new pings to skip
		 * some instances in case they don’t respond.
		 */
		return new AvailabilityFilteringRule();
	}

}